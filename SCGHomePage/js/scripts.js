$(document).ready(function () {
    $(".printmarketing_carousel").owlCarousel({
        nav: true,
        loop: true,
        dots: true,
        items: 1,
        navText: "",
        autoHeight: true,
    });
    $(".printmarketing_carousel").addClass("owl-carousel")

    $(".outdoor_carousel").owlCarousel({
        nav: true,
        loop: true,
        dots: true,
        items: 1,
        navText: "",
        autoHeight: true,
    });
    $(".outdoor_carousel").addClass("owl-carousel")

    $(".portfolio_list").owlCarousel({
        autoHeight: true,
        nav: true,
        loop: true,
        dots: false,
        navText: "",
        responsive: {
            0: {
                items: 1,
            },
            767: {
                items: 2,
            },

            1279: {
                items: 4,
            }
        }
    });
    $(".portfolio_list").addClass("owl-carousel");

    $(".contact_address_photos").owlCarousel({
        nav: true,
        loop: true,
        dots: true,
        items: 1,
        navText: "",
    });
    $(".contact_address_photos").addClass("owl-carousel")

    var owl_carousel_customnav1 = $("#owl_carousel_customnav1");
    owl_carousel_customnav1.owlCarousel({
        nav: true,
        loop: true,
        dots: true,
        items: 1,
        navText: "",
        autoHeight: true,
    });
    owl_carousel_customnav1.addClass("owl-carousel")

    $('#owl_carousel_customnav1 .outdoor_carousel_mobile-nav-prev').click(function() {
        owl_carousel_customnav1.trigger('prev.owl.carousel');
    });

    $('#owl_carousel_customnav1 .outdoor_carousel_mobile-nav-next').click(function() {
        owl_carousel_customnav1.trigger('next.owl.carousel');
    });

    var owl_carousel_customnav2 = $("#owl_carousel_customnav2");
    owl_carousel_customnav2.owlCarousel({
        nav: true,
        loop: true,
        dots: true,
        items: 1,
        navText: "",
        autoHeight: true,
    });
    owl_carousel_customnav2.addClass("owl-carousel")

    $('#owl_carousel_customnav2 .outdoor_carousel_mobile-nav-prev').click(function() {
        owl_carousel_customnav2.trigger('prev.owl.carousel');
    });

    $('#owl_carousel_customnav2 .outdoor_carousel_mobile-nav-next').click(function() {
        owl_carousel_customnav2.trigger('next.owl.carousel');
    });

    var owl_carousel_customnav3 = $("#owl_carousel_customnav3");
    owl_carousel_customnav3.owlCarousel({
        nav: true,
        loop: true,
        dots: true,
        items: 1,
        navText: "",
        autoHeight: true,
    });
    owl_carousel_customnav3.addClass("owl-carousel")

    $('#owl_carousel_customnav3 .outdoor_carousel_mobile-nav-prev').click(function() {
        owl_carousel_customnav3.trigger('prev.owl.carousel');
    });

    $('#owl_carousel_customnav3 .outdoor_carousel_mobile-nav-next').click(function() {
        owl_carousel_customnav3.trigger('next.owl.carousel');
    });

    var owl_carousel_customnav4 = $("#owl_carousel_customnav4");
    owl_carousel_customnav4.owlCarousel({
        nav: true,
        loop: true,
        dots: true,
        items: 1,
        navText: "",
        autoHeight: true,
    });
    owl_carousel_customnav4.addClass("owl-carousel")

    $('#owl_carousel_customnav4 .outdoor_carousel_mobile-nav-prev').click(function() {
        owl_carousel_customnav4.trigger('prev.owl.carousel');
    });

    $('#owl_carousel_customnav4 .outdoor_carousel_mobile-nav-next').click(function() {
        owl_carousel_customnav4.trigger('next.owl.carousel');
    });

    var owl_carousel_customnav5 = $("#owl_carousel_customnav5");
    owl_carousel_customnav5.owlCarousel({
        nav: true,
        loop: true,
        dots: true,
        items: 1,
        navText: "",
        autoHeight: true,
    });
    owl_carousel_customnav5.addClass("owl-carousel")

    $('#owl_carousel_customnav5 .outdoor_carousel_mobile-nav-prev').click(function() {
        owl_carousel_customnav5.trigger('prev.owl.carousel');
    });

    $('#owl_carousel_customnav5 .outdoor_carousel_mobile-nav-next').click(function() {
        owl_carousel_customnav5.trigger('next.owl.carousel');
    });





    $(".portfolio_item").hover(
        function () {
            $(this).closest(".owl-item").css("z-index", "10");
            $(this).closest(".owl-stage-outer").css("overflow", "visible");
        }, function () {
            $(this).closest(".owl-item").css("z-index", "auto");
            $(this).closest(".owl-stage-outer").css("overflow", "hidden");
        }
    );

    $(".with_menu3").on("click", function () {
        $(this).toggleClass("opened");
        $(this).find(".main_menu_3").slideToggle()
    });

    $(".main_menu_1").on("click", function () {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $(this).closest(".main_menu_row_col").find(".main_menu_2").removeClass("opened");
        }
        else {
            $(".main_menu_1").removeClass("active");
            $(this).addClass("active");
            $(".main_menu_2").removeClass("opened");
            $(this).closest(".main_menu_row_col").find(".main_menu_2").addClass("opened");
        }
    });

    $(".header_menu_opener").on("click", function () {
        $(this).toggleClass("opened");
        $('header').toggleClass("opened");
        $(".menu").fadeToggle();
    });

    $(".footer_links_col").on("click", function () {
        $(this).toggleClass("opened");
        $(this).find("ul").slideToggle();
    });


    AOS.init({
        disable: 'mobile',
        once: true,
    });

    $(".parallax").mousemove(function (e) {
        $(this).find(".parallax_img50").parallax(50, e);
        $(this).find(".parallax_img-50").parallax(-50, e);
        $(this).find(".parallax_img-100").parallax(-100, e);
    });

    $.fn.parallax = function (resistance, mouse) {
        $el = $(this);
        TweenLite.to($el, 0.2, {
            x: -((mouse.clientX - (window.innerWidth / 2)) / resistance),
            y: -((mouse.clientY - (window.innerHeight / 2)) / resistance)
        });
    };

    var drop = $('.dropzoneteplate').html();
    if ($('div').is('.dropzonedrag')) {
        $('.dropzonedrag').dropzone({
            url: "/upload",
            paramName: "uploadfile",
            previewTemplate: drop,
            previewsContainer: ".dropzonepreview"
        });
    }
    ;
    if ($('div').is('.dropzonedrag2')) {
        $('.dropzonedrag2').dropzone({
            url: "/upload",
            paramName: "uploadfile",
            previewTemplate: drop,
            previewsContainer: ".dropzonepreview2"
        });
    }
    ;

    //custom tabs control
    $('.awards_timeline a').click(function (e) {
        //get selected href
        var tabHref = $(this).attr('href');

        //set all nav tabs to inactive
        $('.awards_timeline a').removeClass('active');

        //get all nav tabs matching the href and set to active
        $('.awards_timeline a[href="'+tabHref+'"]').addClass('active');

        //active tab
        $('.tab-pane').removeClass('active show');
        $('.' + tabHref).addClass('active show');
    })

    $(".footer_feedback_select").selectmenu();
});

var laprintingWorkflow = $(".laprinting_workflow");
$(window).on("load resize", function () {
    $('.printmarketing_carousel_item').each(function () {
        $(this).css("height", $(this).closest('.printmarketing_section').height() + "px")
    });

    if ($(window).width() < '768') {
        laprintingWorkflow.owlCarousel({
            nav: true,
            loop: true,
            dots: false,
            items: 1,
            navText: "",
        });
        laprintingWorkflow.addClass("owl-carousel");

    } else {
        laprintingWorkflow.removeClass("owl-carousel");
        laprintingWorkflow.trigger('destroy.owl.carousel');
    }

    function is_ios() {
        return (/iPhone|iPad|iPod/i.test(navigator.userAgent));
    };
    if (is_ios()) {
        $("body").addClass("ioscss");
    }
    else {
        $("body").removeClass("ioscss");
    }
});

function initMap() {
    var mapOption = {
        zoom: 15,
        center: {lat: 34.024528, lng: -118.378413},
        styles: [
            {
                "featureType": "all",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "saturation": 36
                    },
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 40
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 17
                    },
                    {
                        "weight": 1.2
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 17
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 29
                    },
                    {
                        "weight": 0.2
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 18
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 19
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 17
                    }
                ]
            }
        ]
    };
    var map = new google.maps.Map(document.getElementById('map'), mapOption);
    var marker = new google.maps.Marker({
        position: {lat: 34.024528, lng: -118.378413},
        map: map,
        icon: 'img/mapmarker.png'
    });

    if ($('div').is('#map2')) {
        var map2 = new google.maps.Map(document.getElementById('map2'), mapOption);
        var marker2 = new google.maps.Marker({
            position: {lat: 34.024528, lng: -118.378413},
            map: map2,
            icon: 'img/mapmarker.png'
        });
    };
}

$(".popup_close").on('click', function () {
    document.getElementById('player').contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
})

