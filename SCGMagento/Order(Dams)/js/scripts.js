$(document).ready(function () {
    $(document).on("click", ".header_user", function () {
        $('.header_user-menu').slideToggle()
    });

    $(document).on("click", ".header_burger", function () {
        $('body').toggleClass('leftpanel-opened')
    });

    $(document).on("click", ".aside_mobile_bg, .aside-opener", function () {
        $('body').removeClass('leftpanel-opened')
    });

    $('.aside_list-list .checkbox').on('change', function () {
        $(this).parent().toggleClass('checked');
    });

    $(document).on("click", ".aside_list-title-opener", function () {
        $(this).parent(".aside_list-group").toggleClass("active");
        $(this).parent().children('.aside_list-list').slideToggle();
    });

    $('.orders_table_inputdate').datepicker({
        firstDay: 1,
        showOtherMonths: true
    });

    $(document).on("click", ".notifications_main_head-search", function () {
        $(this).addClass('mobileinput')
    });

    $(document).on("click", ".notifications_main_head-search button", function () {
        $('.notifications_main_head-search').removeClass('mobileinput')
    });

    $(document).on("click", ".notifications-leftpanel-opener", function () {
        $('.notifications_left').hide();
    });

    $(document).on("click", "#support_menu_opener", function () {
        $('#notifications_menu').slideUp();
        $('#folder_menu').slideUp();
        $('#support_menu').slideToggle();
    });

    $(document).on("click", "#notifications_menu_opener", function () {
        $('#support_menu').slideUp();
        $('#folder_menu').slideUp();
        $('#notifications_menu').slideToggle();
    });

    $(document).on("click", "#folder_menu_opener", function () {
        $('#support_menu').slideUp();
        $('#notifications_menu').slideUp();
        $('#folder_menu').slideToggle();

    });


    $('.select2').select2({
        minimumResultsForSearch: Infinity
    });


    $('.select2_ver2').select2({
        minimumResultsForSearch: Infinity,
        theme: "ver2",
    });

    $('.preflight_selecttext').select2({
        minimumResultsForSearch: Infinity
    });
    $('.preflight_selectcolor').select2({
        templateSelection: formatColor,
        templateResult: formatColor,
        minimumResultsForSearch: Infinity
    });

    $('.accountbilling_company_role_row_user select').select2({
        templateSelection: AccountUser,
        templateResult: AccountUser,
        minimumResultsForSearch: Infinity,
    });

    $('.todolist_filter_select').select2({
        minimumResultsForSearch: Infinity,
        placeholder: "Filter 2",
    });

    $('.invitetomynotes_select_group_learn').hover(
        function () {
            $('.invitetomynotes_permissionlevels').fadeIn();
        },
        function () {
            $('.invitetomynotes_permissionlevels').fadeOut()
        }
    );

    $(document).on("click", ".aside_list2_opener", function () {
        $(this).toggleClass('aside_list2_opener-opened');
        $(this).closest('.aside_list-group').toggleClass('active');
        $(this).closest('.aside_list-group').children('.aside_list2').slideToggle();
    });

    $(document).on("click", ".aside_list3_opener", function () {
        $(this).toggleClass('aside_list3_opener-opened');
        $(this).closest('.aside_list2_item').children('.aside_list2-list').slideToggle();
        $(this).closest('.aside_list2_item').toggleClass('active')
    });

    $(document).on("click", ".header_left-search_opener", function () {
        $('.header_left_search2').toggleClass('visible');
    });



    $(document).on("click", ".menu_close", function () {
        $('.menu_content').slideUp();
    });

    $(document).on("click", ".menu_opener_fade", function () {
        $(this).closest('.menu').children('.menu_content').fadeToggle()
    });

    $(document).on("click", ".menu_close_fade", function () {
        $('.menu_content').fadeOut();
    });



    $('.select_user').select2({
        tags: true,
        theme: "select2_useradd",
        //templateSelection: selectUser,
        //templateResult: selectUser,
        minimumResultsForSearch: Infinity,
    });

    function selectUser(user) {
        if (!user.id) {
            return user.text;
        }
        var $user = $(
            '<div class="select_user_dropdown">' +
            '<div class="select_user_dropdown_photo" style="background-image: url(img/' + user.element.getAttribute('data-photo') + ')">' + user.element.getAttribute('data-initials') + '</div>' //user.element.value.toLowerCase().replace(/\s+/g, '') + ".jpg)"
            +
            '<div class="select_user_dropdown_name">' + user.element.getAttribute('data-name') + '</div>' +
            '<div class="select_user_dropdown_email">' + user.element.getAttribute('data-email') + '</div>'
            +
            '</div>'
        );
        return $user;
    };
});


$(window).on('load resize', function () {
    $('.main-content').css('min-height', $(window).height() - $('header').outerHeight() - 1 + 'px');
    $('.min-height').css('min-height', $(window).height() - $('header').outerHeight() - 1 + 'px');
    $('.min-height-br').css('min-height', $(window).height() - $('header').outerHeight() - $('.breadcrumbs').outerHeight() + 'px');
    $('.inbox_list').css('height', $('.inbox_main').innerHeight() + "px");
    $('.h100').css('height', $(window).height() - $('header').outerHeight() + 'px');
});

function formatColor(color) {
    if (!color.id) {
        return color.text;
    }
    var $color = $(
        '<div class="preflight_color_select">' + '<span style="background-color:' + color.element.value.toLowerCase() + '"class="preflight_color">' + '</span>' + '<span class="preflight_color_name">' + color.text + '</span>' + '</div>'
    );
    return $color;
};

function AccountUser(user) {
    if (!user.id) {
        return user.text;
    }
    var $user = $(
        '<div class="accountbilling_company_role_row_selectuser">' + '<span style="background-image: url(img/' + user.element.value.toLowerCase().replace(/\s+/g, '') + ".jpg)" + '"class="accountbilling_company_role_row_selectuser_photo">' + '</span>' + '<span class="accountbilling_company_role_row_selectuser_name">' + user.text + '</span>' + '</div>'
    );
    return $user;
};


//Size and Position for fixed rightpanel
$(document).ready(function () {
    $(window).on("load scroll resize", function () {
        let headerHeight = $("header").outerHeight();
        let rightPanelFixed = $(".rightpanel_fixed");
        let windowsScrollTop = $(window).scrollTop();

        rightPanelFixed.css("top", headerHeight - windowsScrollTop + "px");
        rightPanelFixed.css("height", $(window).height() - headerHeight + windowsScrollTop + "px");
        if ($(this).scrollTop() > headerHeight) {
            rightPanelFixed.addClass("rightpanel_fixed_s")
        } else {
            rightPanelFixed.removeClass("rightpanel_fixed_s")
        }
    })
});



$(document).ready(function () {
    $(".custom_scrollbar").niceScroll({
        cursorcolor: "#c1c1c1",
        cursorwidth: "6px",
        background: "",
        cursorborderradius: "3px",
        autohidemode: 'leave'
    });

    $(".aside_queue_list").niceScroll({
        cursorcolor: "#c1c1c1",
        cursorwidth: "6px",
        background: "",
        cursorborderradius: "3px",
        autohidemode: 'leave'
    });
});

