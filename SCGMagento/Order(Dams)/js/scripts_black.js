$(document).ready(function () {
    $('.orders_filter_select').select2({
        theme: "select2_scgblack",
        minimumResultsForSearch: Infinity,
    });


    $('.orders_table_select').select2({
        theme: "select2_scgblack_table_select",
        minimumResultsForSearch: Infinity,
    });

    $('.invite_graph_head_time-select select').select2({
        theme: "select2_black",
        minimumResultsForSearch: Infinity,
    });

    $('.header_left-select select').select2({
        theme: "select2_blackhead",
        minimumResultsForSearch: Infinity,
    });

    $('.popup_black_select').select2({
        theme: "popup_black_select",
        minimumResultsForSearch: Infinity,
    });

    $('.popup_black_select_wsearch').select2({
        theme: "popup_black_select",
    });

    $('.popup_newitem_trackinventory_opener').on('click', function () {
        $(".popup_newitem_trackinventory").toggleClass("popup_newitem_trackinventory_opened");
        $(".popup_newitem_trackinventory_opener_p, .popup_newitem_trackinventory_content").fadeToggle();

    });

});


//graphblack for scgmagento
$(document).ready(function () {
    if ($("div").is('#invitegraph_black')) {
        Highcharts.chart('invitegraph_black', {
            title: {
                text: ''
            },
            chart: {
                type: 'areaspline',
                backgroundColor: '#1a1a1b',

            },

            legend: {
                enabled: false
            },
            xAxis: {
                categories: [
                    '100',
                    '500',
                    '1000',
                    '1500',
                    '2000',
                    '2500',
                    '3000',
                    '3500',
                ],
                gridLineColor: '#2e2e2e',
                tickColor: 'rgba(0,0,0,0)',
                lineColor: 'rgba(0,0,0,0)',
            },

            yAxis: {
                title: {
                    text: ''
                },
                gridLineColor: '#2e2e2e',

            },


            credits: {
                enabled: false
            },
            plotOptions: {

            },

            series: [
                {
                    name: 'Large Format',
                    data: [0, 10, 70, 60, 35, 45, 55, 65],
                    color: '#633afd',
                    lineColor: 'null',
                    marker: {
                        enabled: false
                    },
                },
                {
                    name: 'Litho',
                    data: [0, 55, 47, 30, 10, 15, 12, 0],
                    color: '#3e3e3e',
                    lineColor: 'null',
                    marker: {
                        enabled: false
                    },
                },
                {
                    name: 'Digital',
                    data: [0, 15, 12, 5, 25, 40, 30, 0],
                    color: '#f5f6f8',
                    lineColor: 'null',
                    marker: {
                        enabled: false
                    },
                },
            ]
        });
    };

});