$(document).ready(function () {
    $(document).on("click", ".header_user", function () {
        $('.header_user-menu').slideToggle()
    });

    $(document).on("click", ".header_burger", function () {
        $('body').toggleClass('leftpanel-opened')
    });

    $(document).on("click", ".aside_mobile_bg, .aside-opener", function () {
        $('body').removeClass('leftpanel-opened')
    });

    $(document).on("click", ".rightpanel-opener", function () {
        $('.main_section').toggleClass('rightpanel-opened')
    });

    $(document).on("click", ".rightpanel-mobilebg", function () {
        $('.main_section').removeClass('rightpanel-opened')
    });


    $('.aside_list-list .checkbox').on('change', function () {
        $(this).parent().toggleClass('checked');
    });

    $(document).on("click", ".aside_list-title-opener", function () {
        $(this).parent(".aside_list-group").toggleClass("active");
        $(this).parent().children('.aside_list-list').slideToggle();
    });

    $('.category_page-images').masonry({
        itemSelector: '.category_page-img',
        columnWidth: '.category_page-img-grid',
        horizontalOrder: true,
    });

    $(document).on("click", ".catalog2-star, .catalog4-star", function () {
        $(this).toggleClass('selected');
    });

    $(document).on("mouseenter", ".catalog2-star, .catalog4-star", function () {
        $('a.catalog2_item_icon, .catalog4_item').on('click', function (e) {
            e.preventDefault();
        });
    });

    $(document).on("mouseleave", ".catalog2-star, .catalog4-star", function () {
        $('a.catalog2_item_icon, .catalog4_item').off('click');
    });

    $(document).on("click", ".catalog2_item_menu label.checkbox", function () {
        if ($(this).children('input').prop('checked')) {
            $(this).children('input').prop('checked', false);
            $(this).closest('.catalog2_item').removeClass('selected');
            $(this).closest('.brandmpcatalog_group_item').removeClass('selected');

        } else {
            $(this).children('input').prop('checked', true);
            $(this).closest('.catalog2_item').addClass('selected');
            $(this).closest('.brandmpcatalog_group_item').addClass('selected');
            $('.catalog2_item').addClass('menuvisible');
            $('.brandmpcatalog_group_item').addClass('menuvisible');
            $('.catalog2_page').addClass('catalog2_page_panelvisible');
            $('.main_brandmpcatalog').addClass('rightpanel-opened');
            $('.brandmpcatalog_downloadpanel').fadeIn();
            $('body').css("padding-bottom", $('.brandmpcatalog_downloadpanel').innerHeight() + "px")
        }

        if ($('.catalog2_item.selected').length < 1) {
            $('.catalog2_item').removeClass('menuvisible');
            $('.catalog2_page').removeClass('catalog2_page_panelvisible');
            $('.brandmpcatalog_downloadpanel').fadeOut();
            $('body').css("padding-bottom", 0)
        }


        if ($('.brandmpcatalog_group_item.selected').length < 1) {
            $('.brandmpcatalog_group_item').removeClass('menuvisible');
            $('.main_brandmpcatalog').removeClass('rightpanel-opened');
        }
        $('.brandmpcatalog_downloadpanel_select-counter').html($('.catalog2_item.selected').length)
    });



    $(document).on("click", ".catalogSelectAll", function () {
        if ($('.catalog2_item.selected').length == $('.catalog2_item').length) {
            $('.catalog2_item').removeClass('selected');
            $('.catalog2_page').removeClass('catalog2_page_panelvisible');
            $('.catalog2_item input[type="checkbox"]').prop('checked', false);
        } else {
            $('.catalog2_item').addClass('selected');
            $('.catalog2_page').addClass('catalog2_page_panelvisible');
            $('.catalog2_item input[type="checkbox"]').prop('checked', true);
        }
    });




    $(document).on("click", ".breadcrumbs_focus_checkbox", function () {
        $('.catalog2_item').removeClass('selected');
        $('.catalog2_page').removeClass('catalog2_page_panelvisible');
        $('.catalog2_item input[type="checkbox"]').prop('checked', false)
    });

    $(document).on("click", ".BrandFolderSelectAll", function () {
        if ($('.brandmpcatalog_group_item.selected').length == $('.brandmpcatalog_group_item').length) {
            $('.brandmpcatalog_group_item').each(function () {
                $(this).removeClass('selected');
                $(this).removeClass('menuvisible');
                $(this).find("input[type='checkbox']").prop('checked', false);
                //$( selector ).on( "mouseleave", handler )
            })
        } else {
            $('.brandmpcatalog_group_item').each(function () {
                $(this).addClass('selected');
                $(this).addClass('menuvisible');
                $(this).find("input[type='checkbox']").prop('checked', true);
            })
        }


    });


    $(document).on("click", ".catalog2_item_menu-opener", function (e) {
        e.preventDefault();
        var currentMenu = $(this).closest('.catalog2_item');
        $(document).mouseup(function (e) {
            if (currentMenu.has(e.target).length === 0) {
                currentMenu.find('.catalog2_menu').slideUp(150);
                currentMenu.parent().removeClass('opened');
            }
        });
        currentMenu.toggleClass('opened');
        currentMenu.find(".catalog2_item_menu").toggleClass('menuopened');
        currentMenu.find('.catalog2_menu').slideToggle(150);
    });

    $(document).on("click", ".workflow_fields_list li", function () {
        $(this).toggleClass('selected');
    });

    $(document).on("click", ".trafficdashboard_top_buttons ul", function () {
        $(this).toggleClass('opened');
    });

    $(document).on("click", ".trafficdashboard_top_buttons ul li", function () {
        $('.trafficdashboard_top_buttons ul li').removeClass('active');
        $(this).addClass('active');
    });

    $(document).on("click", ".dimagepagepanelgroup-opener", function () {
        $(this).closest('.dimagepagepanel-group').children('.dimagepagepanel-group-content').slideToggle();
        $(this).closest('.dimagepagepanel-group').toggleClass('opened');
    });

    $(document).on("click", ".popup-filelist ul li", function () {
        $(this).toggleClass('selected');
    });

    $(document).on("click", ".companybiling_rightpanel_group-title", function () {
        $(this).closest('.companybiling_rightpanel-group').children('.companybiling_rightpanel-group-content').slideToggle();
    })


    if ($('#datepicker').length) {
        $('#datepicker').datepicker({
            firstDay: 1,
            showOtherMonths: true
        });
    }

    $('.datepicker2').datepicker({
        firstDay: 1,
        showOtherMonths: true,
        dateFormat: 'M ' + 'dd' + ', ' + 'yy',
    });

    $('.datepickerfilter').datepicker({
        firstDay: 1,
        showOtherMonths: true,
        dateFormat: 'MM ' + 'dd' + ',' + 'yy',
    });

    $('.orders_table_inputdate').datepicker({
        firstDay: 1,
        showOtherMonths: true
    });

    $(document).on("click", ".todolist_newtask_table-status", function () {
        $(this).children('.todolist_newtask_table-status-menu').slideToggle();
    });

    $(document).on("click", ".notifications_main_head-search", function () {
        $(this).addClass('mobileinput')
    });

    $(document).on("click", ".notifications_main_head-search button", function () {
        $('.notifications_main_head-search').removeClass('mobileinput')
    });

    $(document).on("click", ".notifications-leftpanel-opener", function () {
        $('.notifications_left').hide();
    });

    $(document).on("click", "#support_menu_opener", function () {
        $('#notifications_menu').slideUp();
        $('#folder_menu').slideUp();
        $('#support_menu').slideToggle();
    });

    $(document).on("click", "#notifications_menu_opener", function () {
        $('#support_menu').slideUp();
        $('#folder_menu').slideUp();
        $('#notifications_menu').slideToggle();
    });

    $(document).on("click", "#folder_menu_opener", function () {
        $('#support_menu').slideUp();
        $('#notifications_menu').slideUp();
        $('#folder_menu').slideToggle();

    });

    $('.todolist_newtask_table_project').hover(
        function () {
            $(this).children('.todolist_newtask_table_project-preview').fadeIn(100);
        },
        function () {
            $(this).children('.todolist_newtask_table_project-preview').fadeOut(100);
        }
    );
    var WorkflowCompanyNav;
    $('.workflow_company_nav a').hover(
        function () {
            WorkflowCompanyNav = $(this).attr('href');
            $(WorkflowCompanyNav).fadeIn()
        },
        function () {
            $(WorkflowCompanyNav).fadeOut();

        }
    );

    $(document).on("click", ".dimagepage_main_img_comment_opener", function () {
        $(this).closest('.dimagepage_main_img_comment').toggleClass('active');
        $(this).closest('.dimagepage_main_img_comments').toggleClass('focused');
    });

    $(document).on("click", ".dimagepagepanel_tabs a", function () {
        $('.dimagepage_main_img_comments').fadeOut();
    });

    $(document).on("click", "#commentsvisible a", function () {
        $('.dimagepage_main_img_comments').fadeIn();
    });
    /*
    $(document).on("click", ".workflow_company_group-title_menu_opener", function () {
        $(this).parent().children('.workflow_company_group-title_menu').slideToggle();
    });
    */
    $('.businesscards_carousel').owlCarousel({
        nav: false,
        loop: true,
        dots: true,
        navText: false,
        items: 1,
        margin: 0,
    });

    $('.cardnumber').mask('0000   0000   0000   0000');
    $('.cardcvv').mask('000');
    $('.cardmm').mask('00');
    $('.cardyy').mask('00');

    var pass;
    $(document).on("click", ".popup_group_viewpassword", function () {
        pass = $(this).parent().children('input');
        pass.attr('type', pass.attr('type') === 'password' ? 'text' : 'password');
    });

    $('.select2').select2({
        minimumResultsForSearch: Infinity
    });


    $('.select2_ver2').select2({
        minimumResultsForSearch: Infinity,
        theme: "ver2",
    });

    $('.preflight_selecttext').select2({
        minimumResultsForSearch: Infinity
    });
    $('.preflight_selectcolor').select2({
        templateSelection: formatColor,
        templateResult: formatColor,
        minimumResultsForSearch: Infinity
    });

    $('.accountbilling_company_role_row_user select').select2({
        templateSelection: AccountUser,
        templateResult: AccountUser,
        minimumResultsForSearch: Infinity,
    });

    $('.todolist_filter_select').select2({
        minimumResultsForSearch: Infinity,
        placeholder: "Filter 2",
    });

    $(document).on("click", ".fileoverview_rp_user-menu", function () {
        $(this).children('.fileoverview_rp_user_menu').slideToggle();

    });

    $(document).on("click", ".documentssent_table_action_opener", function () {
        $(this).closest('.documentssent_table_action').children('.documentssent_table_action_menu').slideToggle()
    });

    $('.invitetomynotes_select_group_learn').hover(
        function () {
            $('.invitetomynotes_permissionlevels').fadeIn();
        },
        function () {
            $('.invitetomynotes_permissionlevels').fadeOut()
        }
    );

    $(document).on("click", ".inbox_list_item_title", function () {
        $('.inbox').addClass('visible')
    });

    $(document).on("click", ".inbox_head_closemobile", function () {
        $('.inbox').removeClass('visible')
    });


    $(document).on("click", ".inbox_head_search", function () {
        $(this).addClass('opened')
    });

    $(document).on("click", ".aside_list2_opener", function () {
        $(this).toggleClass('aside_list2_opener-opened');
        $(this).closest('.aside_list-group').toggleClass('active');
        $(this).closest('.aside_list-group').children('.aside_list2').slideToggle();
    });

    $(document).on("click", ".aside_list3_opener", function () {
        $(this).toggleClass('aside_list3_opener-opened');
        $(this).closest('.aside_list2_item').children('.aside_list2-list').slideToggle();
        $(this).closest('.aside_list2_item').toggleClass('active')
    });

    $(document).on("click", ".header_left-search_opener", function () {
        $('.header_left_search2').toggleClass('visible');
    });

    $(document).on("click", ".producttemplates_product_menuopener", function () {
        $(this).closest('.producttemplates_product_title').children('.producttemplates_product_menu').slideToggle();
    });

    $(document).on("click", ".producttemplates_product_menu", function () {
        $(this).slideUp();
    });


    //editcorporate tabs
    var EditcorporateTabs;
    $('.editcorporate_nav a').on('click', function () {
        EditcorporateTabs = $(this).attr('href');
        $('.editcorporate_nav a').removeClass('active');
        $(this).addClass('active');
        $('.editcorporate_col').fadeOut();
        $('.editcorporate_col').removeClass('active');
        $(EditcorporateTabs).fadeIn();
    });

    $(document).on("click", ".filters_rp_group_title", function () {
        $(this).toggleClass('opened')
        $(this).closest('.filters_rp_group').children('.filters_rp_group_content').slideToggle()
    });

    $(document).on("click", ".catalog2_folders_list", function () {
        $('.catalog2_folders').toggleClass("focused")
        $(this).children('.catalog2_folders_list_ul').slideToggle()
    });

    $(document).on("click", ".todolist_main .title_i", function () {
        $(this).toggleClass('active');
        $('.filters_rp').toggleClass('opened');
    });
    $(document).on("click", ".docs_editor_comment_icon", function () {
        DocsEditorCommentInner = $(this).closest('.docs_editor_comment').children('.docs_editor_comment_inner');
        if ($(this).closest('.docs_editor_comment').hasClass('opened')) {
            $('.docs_editor_comment').removeClass('opened');
            $('.docs_editor_comment_inner').hide();
        } else {
            $('.docs_editor_comment').removeClass('opened');
            $('.docs_editor_comment_inner').hide();
            $(this).closest('.docs_editor_comment').toggleClass('opened');
            $(this).closest('.docs_editor_comment').children('.docs_editor_comment_inner').fadeToggle();
        }
        ;
        DocsEditorPosition()
    });

    var PopupMember, PopupMemberCurrent;
    $(".addmembers_popup_select").select2({
        placeholder: "Select a user"
    });

    $(document).on("click", ".addmembers_popup_member", function () {
        $(this).toggleClass('addmembers_popup_member_added');
        PopupMember = $(this).attr('data-item')
        PopupMemberCurrent = $('.addmembers_popup_select option:contains(' + PopupMember + ')');
        if (PopupMemberCurrent.prop('selected')) {
            PopupMemberCurrent.removeAttr("selected ");
        } else {
            PopupMemberCurrent.attr("selected", "");
        }
        ;
        $(".addmembers_popup_select").trigger("change")

    });

    $('.addmembers_popup_select').on("select2:select", function (e) {
        var PopupMemberNotSelected = e.params.data.id;
        $('.addmembers_popup_member_name:contains(' + PopupMemberNotSelected + ')').closest('.addmembers_popup_member').addClass('addmembers_popup_member_added')
    }).trigger("change");

    $('.addmembers_popup_select').on("select2:unselect", function (e) {
        var PopupMemberNotSelected = e.params.data.id;
        $('.addmembers_popup_member_name:contains(' + PopupMemberNotSelected + ')').closest('.addmembers_popup_member').removeClass('addmembers_popup_member_added')
    }).trigger("change");

    $(document).on("click", ".menu_opener", function (e) {
        e.preventDefault();
        var currentMenu = $(this).closest('.menu');
        $(document).mouseup(function (e) {
            if (currentMenu.has(e.target).length === 0) {
                currentMenu.find('.menu_content').slideUp(150);
                currentMenu.find('.menu_content').removeClass("opened");
            }
        });
        $(this).toggleClass("opened");
        $(this).closest('.menu').find('.menu_content').toggleClass("opened");
        $(this).closest('.menu').find('.menu_content').slideToggle(150);
    });


    $('.brandmpcatalog2_group_body-carousel').owlCarousel({
        nav: true,
        loop: false,
        dots: false,
        navText: false,
        responsive: {
            0: {
                items: 2,
                margin: 12,
            },

            768: {
                autoWidth: true,
                margin: 18,
            }
        }
    });
    $(document).on("click", ".brandmpcatalog2_checkbox", function () {
        if ($(this).children('input').prop('checked')) {
            $(this).children('input').prop('checked', false);
            $(this).closest('.brandmpcatalog2_group_item2').removeClass('selected');
        } else {
            $(this).children('input').prop('checked', true);
            $(this).closest('.brandmpcatalog2_group_item2').addClass('selected')
            $('.brandmpcatalog_downloadpanel').fadeIn();
            $('body').css("padding-bottom", $('.brandmpcatalog_downloadpanel').innerHeight() + "px")

        }
        if ($('.brandmpcatalog2_group_item2.selected').length < 1) {
            $('.brandmpcatalog_downloadpanel').fadeOut();
            $('body').css("padding-bottom", 0)
        }
        ;
        $('.brandmpcatalog_downloadpanel_select-counter').html($('.brandmpcatalog2_group_item2.selected').length)
    });

    $(document).on("click", ".brandmpcatalog_downloadpanel_selectall_checkbox", function () {
        if ($(this).hasClass('checked')) {
            $(this).removeClass('checked');
            $('.brandmpcatalog2_group_item2').removeClass('selected');
            $('.brandmpcatalog2_checkbox').children('input').prop('checked', false);
            $('.catalog_quest_selectall').removeClass('checked');
            $('.catalog2_item').removeClass('selected');
            $('.catalog2_item_menu .checkbox').children('input').prop('checked', false);
            $('.brandmpcatalog_group_item').removeClass('selected');
            $('.brandmpcatalog_group_item').removeClass('menuvisible');
            $('.brandmpcatalog_group_item .checkbox').children('input').prop('checked', false);

        } else {
            $(this).addClass('checked');
            $('.brandmpcatalog2_group_item2').addClass('selected');
            $('.brandmpcatalog2_checkbox').children('input').prop('checked', true);
            $('.catalog_quest_selectall').addClass('checked');
            $('.catalog2_item').addClass('selected');
            $('.catalog2_item_menu .checkbox').children('input').prop('checked', true);
            $('.brandmpcatalog_group_item').addClass('selected');
            $('.brandmpcatalog_group_item').addClass('menuvisible');
            $('.brandmpcatalog_group_item').find('input').prop('checked', true);
        }
        $('.brandmpcatalog_downloadpanel_select-counter').html($('.brandmpcatalog2_group_item2.selected').length);
        $('.brandmpcatalog_downloadpanel_select-counter').html($('.catalog2_item.selected').length);
        $('.brandmpcatalog_downloadpanel_select-counter').html($('.brandmpcatalog_group_item.selected').length);
    });

    $(document).on("click", ".catalog_quest_selectall", function () {
        if ($(this).hasClass('checked')) {
            $(this).removeClass('checked');
            $('.brandmpcatalog2_group_item2').removeClass('selected');
            $('.brandmpcatalog2_checkbox').children('input').prop('checked', false);
            $('.brandmpcatalog_downloadpanel_selectall_checkbox').removeClass('checked');

        } else {
            $(this).addClass('checked');
            $('.brandmpcatalog2_group_item2').addClass('selected');
            $('.brandmpcatalog2_checkbox').children('input').prop('checked', true);
            $('.brandmpcatalog_downloadpanel_selectall_checkbox').addClass('checked');
            $('.brandmpcatalog_downloadpanel').fadeIn();
            $('body').css("padding-bottom", $('.brandmpcatalog_downloadpanel').innerHeight() + "px")
        }
        ;
        if ($('.brandmpcatalog2_group_item2.selected').length < 1 && $('.catalog2_item.selected').length < 1) {
            $('.brandmpcatalog_downloadpanel').fadeOut();
            $('body').css("padding-bottom", 0)
        }
        ;
        $('.brandmpcatalog_downloadpanel_select-counter').html($('.brandmpcatalog2_group_item2.selected').length)
    });



    $('.brandmpcatalog_downloadpanel').hover(
        function () {
            if ($('.brandmpcatalog2_group_item2.selected').length < 1 && $('.catalog2_item.selected').length < 1 && $('.brandmpcatalog_group_item.selected').length < 1 ) {
                $('.brandmpcatalog_downloadpanel').fadeOut();
            }
            ;
        });


    $('.dashboardgraph_item_progress_graph').circleProgress({
        value: 0.65,
        size: 230,
        startAngle: -Math.PI / 2,
        fill: '#633afd',
        emptyFill: '#e1e4eb',
        thickness: 10,
    }).on('circle-animation-progress', function (event, progress, stepValue) {
        $(this).find('.dashboardgraph_item_progress_graph-value span').html(stepValue.toFixed(2).substr(2) + '<i>%</i>');
    });

    $('.dashboardgraph_item_content_progress').circleProgress({
        value: 0.96,
        size: 84,
        startAngle: -Math.PI / 2,
        fill: '#633afd',
        emptyFill: '#e1e4eb',
        thickness: 4,
    }).on('circle-animation-progress', function (event, progress, stepValue) {
        $(this).find('.dashboardgraph_item_content_progress-val').html(stepValue.toFixed(2).substr(2) + '<i>%</i>');
    });

    $('#uislider').slider({
        range: "min",
        value: 50,
        min: 0,
        max: 100,
        slide: function () {
            $('#uisliderValue').val($('#uislider').slider('value') + '%')
            //$('#uislider').slider
        }
    });

    function update() {
        var tasks_time = $('#tasks_time').slider('value');
        var tasks_done = $('#tasks_done').slider('value');
        var total_cost = (tasks_time * 4 * tasks_done) / (tasks_done * 3);
        var credits_needed = Math.round((total_cost / 10) + 1);
        $("#total_cost").text(total_cost.toFixed(2));
        $("#curr-tasks_time").text(tasks_time);
        $("#curr-tasks_done").text(tasks_done);
        $("#credits_needed").text(credits_needed.toFixed(0));

    }


    if ($("div").is('#dashboardgraph_item_graph')) {
        Highcharts.chart('dashboardgraph_item_graph', {
            title: {
                text: ''
            },
            chart: {
                type: 'areaspline'
            },
            legend: {
                enabled: false
            },
            xAxis: {
                categories: [
                    '100',
                    '500',
                    '1000',
                    '1500',
                    '2000',
                    '2500',
                    '3000',
                    '3500',
                ],
            },

            yAxis: {
                title: {
                    text: ''
                },
            },


            credits: {
                enabled: false
            },
            plotOptions: {},

            series: [
                {
                    name: 'Base',
                    data: [0, 10, 70, 60, 35, 45, 55, 65],
                    color: '#f5f6f8',
                    lineColor: 'null',
                    marker: {
                        enabled: false
                    },
                },
                {
                    name: 'Perfomance',
                    data: [0, 55, 47, 30, 10, 15, 12, 0],
                    color: '#bbabfa',
                    lineColor: 'null',
                    marker: {
                        enabled: false
                    },
                },
                {
                    name: 'First graph',
                    data: [0, 15, 12, 5, 25, 40, 30, 0],
                    color: '#633afd',
                    lineColor: 'null',
                    marker: {
                        enabled: false
                    },
                },
            ]
        });
    }
    ;

    $(document).on("click", ".catalog4_uploadform_slidetoggle", function () {
        $(this).toggleClass('small')
        $('.catalog4_uploadform_body').slideToggle(300);
        setTimeout(function () {
            Catalog4Paddin()
        }, 300);

    });
    Catalog4Paddin();

    function Catalog4Paddin() {
        var Catalog4Height = $('.catalog4_uploadform').outerHeight()
        if ($('.catalog4_uploadform').is(':visible')) {
            $('.main-content').css('padding-bottom', Catalog4Height + 'px');
            $('aside').css('padding-bottom', Catalog4Height + 'px');
        } else {
            $('.main-content').css('padding-bottom', 0);
            $('aside').css('padding-bottom', 0);
        }
    };

    $(document).on("click", ".dimagepage_main_img_comment_reply_add", function () {
        $('.dimagepage_main_img_comment_reply_users').slideToggle()
    });

    $('.dimagepage_main_img_comment_reply_input').on('input', function () {
        if ($(this).text() == "@") {
            $('.dimagepage_main_img_comment_reply_users').slideDown()
        } else {
            $('.dimagepage_main_img_comment_reply_users').slideUp()
        }
    });

    $(document).on("click", ".dimagepage_main_img_comment_reply_user", function () {
        $('.dimagepage_main_img_comment_reply_input').text("");
        $('.dimagepage_main_img_comment_reply-textarea').append('<span class="dimagepage_main_img_comment_reply_selected">' + $(this).attr('data-name') + '</span>');
        $('.dimagepage_main_img_comment_reply_users').slideUp()
    });

    var drop = $('.dropzoneteplate').html();
    if ($('div').is('#dropzonedrag')) {
        $('#dropzonedrag, #catalog_project_mask').dropzone({
            url: "/upload",
            paramName: "uploadfile",
            maxThumbnailFilesize: 5,
            previewTemplate: drop,
            previewsContainer: ".dropzonepreview"
        });

        $('#dropzonedrag').on('drop', function () {
            $('.catalog4_uploadform_body').slideDown()
        });
    }
    ;


    $(document).on("click", "#LibraryTab", function () {
        $('a[href="#librarytab"]').tab('show')
    });


    $(".catalog4_item_dragdable").draggable({
        connectToSortable: ".catalog4_item",
        revert: "invalid"
    });

    $(".catalog_project, .catalog_project2").draggable({
        connectToSortable: ".catalog_projects",
        revert: "invalid",
        connectToSortable: ".catalog2_folders",

    });

    $('.catalog_project_bottom_panel_button-link').hover(
        function () {
            $(this).closest(".catalog_project").children('.catalog_project_linkhover').fadeIn()
        },
        function () {
            $(this).closest(".catalog_project").children('.catalog_project_linkhover').fadeOut()
        });
    $('.catalog_project2_bottom_panel_button-link').hover(
        function () {
            $(this).closest(".catalog_project2").children('.catalog_project_linkhover').fadeIn()
        },
        function () {
            $(this).closest(".catalog_project2").children('.catalog_project_linkhover').fadeOut()
        });

    $(document).on("click", ".catalog6_rp_opener, .catalog6_more", function () {
        $(".catalog6_rp_opener").toggleClass("hide");
        $('.main_section').toggleClass('rightpanel-opened')

    });

    $(document).on("click", ".menu_close", function () {
        $('.menu_content').slideUp();
    });

    $(document).on("click", ".menu_opener_fade", function () {
        $(this).closest('.menu').children('.menu_content').fadeToggle()
    });

    $(document).on("click", ".menu_close_fade", function () {
        $('.menu_content').fadeOut();
    });

    $(document).on("click", ".catalog_project2_bottom_panel_button-check", function () {
        if ($(this).children('input').prop('checked')) {
            $(this).children('input').prop('checked', false);
            $(this).closest('.catalog_project2').removeClass('selected');

        } else {
            $(this).children('input').prop('checked', true);
            $(this).closest('.catalog_project2').addClass('selected')
            $('.main_section').addClass('rightpanel-opened');
            $('.catalog6_rp_opener').addClass("hide");
        }

        if ($('.catalog_project2.selected').length < 1) {
            $('.main_section').removeClass('rightpanel-opened')
            $('.catalog6_rp_opener').removeClass("hide");
        }
        ;
    });

    $(document).on("click", ".catalog_project_bottom_panel_button-check", function () {
        if ($(this).children('input').prop('checked')) {
            $(this).children('input').prop('checked', false);
            $(this).closest('.catalog_project').removeClass('selected');

        } else {
            $(this).children('input').prop('checked', true);
            $(this).closest('.catalog_project').addClass('selected')
            $('.main_section').addClass('rightpanel-opened');
            $('.catalog6_rp_opener').addClass("hide");
        }

        if ($('.catalog_project.selected').length < 1) {
            $('.main_section').removeClass('rightpanel-opened')
            $('.catalog6_rp_opener').removeClass("hide");
        }
        ;

        if ($('.catalog_project.selected').length == catalogProjectLength) {
            $('#catalog6-selectall input').prop('checked', true)
        } else {
            $('#catalog6-selectall input').prop('checked', false)
        }
    });

    var catalogProjectLength = $(".catalog_project").length;
    $(document).on("click", "#catalog6-selectall input", function () {
        if ($(this).prop('checked')) {
            $('.catalog_project_bottom_panel_button-check').children('input').prop('checked', true);
            $('.catalog_project').addClass('selected')
            $('.main_section').addClass('rightpanel-opened');
            $('.catalog6_rp_opener').addClass("hide");
        } else {
            $('.catalog_project_bottom_panel_button-check').children('input').prop('checked', false);
            $('.catalog_project').removeClass('selected');
            $('.main_section').removeClass('rightpanel-opened');
            $('.catalog6_rp_opener').removeClass("hide");
        }

        if ($('.catalog_project.selected').length < 1) {
            $('.main_section').removeClass('rightpanel-opened')
            $('.catalog6_rp_opener').removeClass("hide");
        }
        ;

    });

    $('.select_user').select2({
        tags: true,
        theme: "select2_useradd",
        //templateSelection: selectUser,
        //templateResult: selectUser,
        minimumResultsForSearch: Infinity,
    });

    function selectUser(user) {
        if (!user.id) {
            return user.text;
        }
        var $user = $(
            '<div class="select_user_dropdown">' +
            '<div class="select_user_dropdown_photo" style="background-image: url(img/' + user.element.getAttribute('data-photo') + ')">' + user.element.getAttribute('data-initials') + '</div>' //user.element.value.toLowerCase().replace(/\s+/g, '') + ".jpg)"
            +
            '<div class="select_user_dropdown_name">' + user.element.getAttribute('data-name') + '</div>' +
            '<div class="select_user_dropdown_email">' + user.element.getAttribute('data-email') + '</div>'
            +
            '</div>'
        );
        return $user;
    };


});


$(window).on('load resize', function () {
    $('.main-content').css('min-height', $(window).height() - $('header').outerHeight() - 1 + 'px');
    $('.min-height').css('min-height', $(window).height() - $('header').outerHeight() - 1 + 'px');
    $('.min-height-br').css('min-height', $(window).height() - $('header').outerHeight() - $('.breadcrumbs').outerHeight() + 'px');
    $('.inbox_list').css('height', $('.inbox_main').innerHeight() + "px");
    $('.h100').css('height', $(window).height() - $('header').outerHeight() + 'px');
});

$(window).on('load resize scroll', function () {
    $('.filetasks_boards').css('height', $(window).height() - $('header').height() - $('.filetasks_head').height() - 34 + 'px');
});

function formatColor(color) {
    if (!color.id) {
        return color.text;
    }
    var $color = $(
        '<div class="preflight_color_select">' + '<span style="background-color:' + color.element.value.toLowerCase() + '"class="preflight_color">' + '</span>' + '<span class="preflight_color_name">' + color.text + '</span>' + '</div>'
    );
    return $color;
};

function AccountUser(user) {
    if (!user.id) {
        return user.text;
    }
    var $user = $(
        '<div class="accountbilling_company_role_row_selectuser">' + '<span style="background-image: url(img/' + user.element.value.toLowerCase().replace(/\s+/g, '') + ".jpg)" + '"class="accountbilling_company_role_row_selectuser_photo">' + '</span>' + '<span class="accountbilling_company_role_row_selectuser_name">' + user.text + '</span>' + '</div>'
    );
    return $user;
};

var DocsEditorBottom, DocsEditorCommentInner, DocsEditorCommentInnerBottom, DocsEditorRight, DocsEditorCommentInnerRight

function DocsEditorPosition() {
    DocsEditorBottom = $('.docs').offset().top + $('.docs').height();
    DocsEditorRight = $('.docs').offset().left + $('.docs').width();
    DocsEditorCommentInnerBottom = DocsEditorCommentInner.offset().top + DocsEditorCommentInner.height();
    DocsEditorCommentInnerRight = DocsEditorCommentInner.offset().left + DocsEditorCommentInner.width();
    if (DocsEditorCommentInnerBottom >= DocsEditorBottom) {
        $('.docs_editor_comment').addClass('docs_editor_comment_bottom')
    } else {
        $('.docs_editor_comment').removeClass('docs_editor_comment_bottom')
    }

    if (DocsEditorCommentInnerRight >= DocsEditorRight) {
        $('.docs_editor_comment').addClass('docs_editor_comment_right')
    } else {
        $('.docs_editor_comment').removeClass('docs_editor_comment_right')
    }


}

$(document).ready(function () {
    if ($("div").is('#invitegraph')) {
        Highcharts.chart('invitegraph', {
            title: {
                text: ''
            },
            chart: {
                type: 'areaspline'
            },
            legend: {
                enabled: false
            },
            xAxis: {
                categories: [
                    '100',
                    '500',
                    '1000',
                    '1500',
                    '2000',
                    '2500',
                    '3000',
                    '3500',
                ],
            },

            yAxis: {
                title: {
                    text: ''
                },
            },


            credits: {
                enabled: false
            },
            plotOptions: {},

            series: [
                {
                    name: 'Base',
                    data: [0, 10, 70, 60, 35, 45, 55, 65],
                    color: '#f5f6f8',
                    lineColor: 'null',
                    marker: {
                        enabled: false
                    },
                },
                {
                    name: 'Perfomance',
                    data: [0, 55, 47, 30, 10, 15, 12, 0],
                    color: '#bbabfa',
                    lineColor: 'null',
                    marker: {
                        enabled: false
                    },
                },
                {
                    name: 'First graph',
                    data: [0, 15, 12, 5, 25, 40, 30, 0],
                    color: '#633afd',
                    lineColor: 'null',
                    marker: {
                        enabled: false
                    },
                },
            ]
        });
    }
    ;

});

//Size and Position for fixed rightpanel
$(document).ready(function () {
    $(window).on("load scroll resize", function () {
        let headerHeight = $("header").outerHeight();
        let rightPanelFixed = $(".rightpanel_fixed");
        let windowsScrollTop = $(window).scrollTop();

        rightPanelFixed.css("top", headerHeight - windowsScrollTop + "px");
        rightPanelFixed.css("height", $(window).height() - headerHeight + windowsScrollTop + "px");
        if ($(this).scrollTop() > headerHeight) {
            rightPanelFixed.addClass("rightpanel_fixed_s")
        } else {
            rightPanelFixed.removeClass("rightpanel_fixed_s")
        }
    })
});

//brandmpcatalog_searh_select
$(document).ready(function () {
    $('.brandmpcatalog_searh_select').select2({
        minimumResultsForSearch: Infinity,
        theme: "brandmpcatalogselect",
    });
});

//Show All File Queue
$(document).ready(function () {
    $(document).on("click", ".aside_queue_showall", function () {
        $(this).fadeOut(150);
        $(".aside_queue_list").css("max-height", "none");

    })
});

//ClipboardJS;
$(document).ready(function () {
    new ClipboardJS('.copyToClipboard');
});

//Select User Single
$(document).ready(function () {
    $('.selectUserSingle').select2({
        minimumResultsForSearch: 1,
        theme: "selectUserSingle",
        templateSelection: selectUserSingle,
        templateResult: selectUserSingle,
        tags: true
    });

    $('.selectUserMultiple').select2({
        minimumResultsForSearch: 1,
        theme: "selectUserMultiple",
        templateSelection: selectUserMultiple,
        templateResult: selectUserSingle,
        tags: true
    });

    $('.selectText').select2({
        minimumResultsForSearch: Infinity,
        theme: "selectText",
    });
});

function selectUserSingle(user) {
    let $user;
    if (!user.id) {
        return user.text;
    }

    if (typeof $(user.element).attr('data-userphoto') != "undefined") {
        $user = $(
            '<div class="selectUserSingle">' +
            '<span class="selectUserSinglePhoto" style="background-image: url(img/' + user.element.getAttribute('data-userphoto') + ')"></span>' +
            '<span class="selectUserSingleName">' + user.text + '</span>' +
            '</div>'
        );
    } else {
        $user = $(
            '<div class="selectUserSingle">' +
            '<span class="selectUserSinglePhoto" style="background-image: url(img/unknownuser.png)"></span>' +
            '<span class="selectUserSingleName">' + user.text + '</span>' +
            '</div>'
        );
    }

    return $user;
};

function selectUserMultiple(user) {
    let $user;
    if (!user.id) {
        return user.text;
    }

    if (typeof $(user.element).attr('data-userphoto') != "undefined") {
        $user = $(
            '<div class="selectUserMultiple">' +
            '<span class="selectUserMultiplePhoto" style="background-image: url(img/' + user.element.getAttribute('data-userphoto') + ')"></span>' +
            '<span class="selectUserMultipleName">' + user.element.value + '</span>' +
            '</div>'
        );
    } else {
        $user = $(
            '<div class="selectUserMultiple">' +
            '<span class="selectUserMultiplePhoto" style="background-image: url(img/unknownuser.png)"></span>' +
            '<span class="selectUserMultipleName">' + user.text + '</span>' +
            '</div>'
        );
    }

    return $user;
};


//autocomplete
$(document).ready(function () {
    $(".autocomplete").autocomplete({
        source: availableTags
    });
});

var availableTags = [
    "very long text for demonstration few rows Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
    "AppleScript",
    "Asp",
    "BASIC",
    "C",
    "C++",
    "Clojure",
    "COBOL",
    "ColdFusion",
    "Erlang",
    "Fortran",
    "Groovy",
    "Haskell",
    "Java",
    "JavaScript",
    "Lisp",
    "Perl",
    "PHP",
    "Python",
    "Ruby",
    "Scala",
    "Scheme"
];

//change content image preview
$(document).ready(function () {
    $(document).on("click", ".dimagepagepanel_version_img", function () {
        let currentImage = $(this).children("img").attr("src");
        $(".dimagepage_main-img img").attr("src", currentImage);
    })
});

$(document).ready(function () {
    $(document).on("click", ".renameFunc", function () {
        let currentrename = $(this).closest(".catalog2_item").find(".toRename");
        currentrename.attr("contenteditable", "true");
        currentrename.focus();
        $(".catalog2_menu").hide();
        $(".catalog2_item_menu").removeClass('menuopened');
        currentrename.selectText();
    });

    $(".toRename").keydown(function (e) {
        if (e.keyCode === 13) {
            $(this).attr("contenteditable", "false");
        }
    });


    $(document).on("click", ".deleted_alert_close", function () {
        $(".deleted_alert").fadeOut();
    });


});

jQuery.fn.selectText = function () {
    var element = this[0];
    if (document.body.createTextRange) {
        var range = document.body.createTextRange();
        range.moveToElementText(element);
        range.select();
    } else if (window.getSelection) {
        var selection = window.getSelection();
        var range = document.createRange();
        range.selectNodeContents(element);
        selection.removeAllRanges();
        selection.addRange(range);
    }
};

$(document).ready(function () {
    $(".custom_scrollbar").niceScroll({
        cursorcolor: "#c1c1c1",
        cursorwidth: "6px",
        background: "",
        cursorborderradius: "3px",
        autohidemode: 'leave'
    });

    $(".aside_queue_list").niceScroll({
        cursorcolor: "#c1c1c1",
        cursorwidth: "6px",
        background: "",
        cursorborderradius: "3px",
        autohidemode: 'leave'
    });
});


$(document).ready(function () {
    $(".catalog2_draggable").draggable({
        revert: "invalid",
        distance: 100,

        start: function() {
            $(".breadcrumbs").addClass("draggable_drag")
        },
        stop: function() {
            $(".breadcrumbs").removeClass("draggable_drag")
        },
    });
    $(".catalog2_droppable").droppable({
        drop: function (event, ui) {
            removeDroppedItem(ui.draggable);
        }
    });

    $(".breadcrumbs_droppable").droppable({
        tolerance: "touch",
        over: function( event, ui ) {
            $(".breadcrumbs_droppable_2").show();
        },
        deactivate: function( event, ui ) {
            $(".breadcrumbs_droppable_2").hide();
        }
    });

    $(".breadcrumbs_droppable2").droppable({
        drop: function (event, ui) {
            removeDroppedItem(ui.draggable);
            $(".breadcrumbs_droppable_2").hide();
        },
    });

    $(document).on("click", ".breadcrumbs-opener", function () {
        $(this).toggleClass("opened");
        $(this).closest("li").find(".breadcrumbs_droppable_2").slideToggle(150);
    });
});

function removeDroppedItem($item) {
    $item.closest(".catalog2_item").fadeOut();
}


$(document).on("click", ".brandmpcatalog_group_item label.checkbox", function () {
    if ($(this).find('input').prop('checked')) {
        $(this).find('input').prop('checked', false);
        $(this).closest('.brandmpcatalog_group_item').removeClass('selected');

    } else {
        $(this).find('input').prop('checked', true);
        $(this).closest('.brandmpcatalog_group_item').addClass('selected');
        $('.brandmpcatalog_group_item').addClass('menuvisible');
        $('.main_brandmpcatalog').addClass('brandmpcatalog_panelvisible');
        $('.brandmpcatalog_downloadpanel').fadeIn();
        $('body').css("padding-bottom", $('.brandmpcatalog_downloadpanel').innerHeight() + "px")
    }

    if ($('.brandmpcatalog_group_item.selected').length < 1) {
        $('.brandmpcatalog_group_item').removeClass('menuvisible');
        $('.main_brandmpcatalog').removeClass('brandmpcatalog_panelvisible');
        $('.brandmpcatalog_downloadpanel').fadeOut();
        $('body').css("padding-bottom", 0)
    }
    $('.brandmpcatalog_downloadpanel_select-counter').html($('.brandmpcatalog_group_item.selected').length)
});




$(document).on("mouseleave", ".brandmpcatalog_group_item", function (e) {
    $(this).find('.menu_content').hide();
    $(this).find('.menu_content').removeClass("opened");
    $(this).find('.menu_opener').removeClass("opened");
});
