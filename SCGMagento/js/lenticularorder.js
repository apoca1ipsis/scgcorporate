$(document).ready(function () {
    $('textarea[name="project"]').on('change', function () {
        if ($('textarea[name="project"]').val() !== '') {
            $("#order_input_text").addClass("order_input_group_success");
            $("#order_input_text").removeClass("order_input_group_alert");
        } else {
            $("#order_input_text").removeClass("order_input_group_success");
        }
    });

    $('input[name="width"]').on('change', function () {
        if ($('input[name="width"]').val() !== '' && $('input[name="width"]').val() > 0) {
            $("#order_input_width").addClass("order_input_group_success");
            $("#order_input_width").removeClass("order_input_group_alert");
        } else {
            $("#order_input_width").removeClass("order_input_group_success");
        }
    });

    $('input[name="height"]').on('change', function () {
        if ($('input[name="height"]').val() !== '' && $('input[name="height"]').val() > 0) {
            $("#order_input_height").addClass("order_input_group_success");
            $("#order_input_height").removeClass("order_input_group_alert");
        } else {
            $("#order_input_height").removeClass("order_input_group_success");
        }
    });
});


$("#form").on('submit', function (e) {
    e.preventDefault();
    $(".order_input_group").removeClass("order_input_group_alert");

    let checkboxesLPI = [];
    $('input[name="lpi[]"]:checked').each(function () {
        checkboxesLPI.push($(this).val());
    });

    let checkboxesEffects = [];
    $('input[name="effect[]"]:checked').each(function () {
        checkboxesEffects.push($(this).val());
    });

    if ($('textarea[name="project"]').val() == '') {
        $("#order_input_text").addClass("order_input_group_alert");
    }

    if ($('input[name="width"]').val() == '' || $('input[name="width"]').val() < 1) {
        $("#order_input_width").addClass("order_input_group_alert");
    }

    if ($('input[name="height"]').val() == '' || $('input[name="height"]').val() < 1) {
        $("#order_input_height").addClass("order_input_group_alert");
    } else {
        $.ajax({
            url: 'phpmailer.php',
            type: 'POST',
            contentType: false,
            processData: false,
            data: {
                project: this.project.value,
                width: this.width.value,
                height: this.height.value,
                lpi: checkboxesLPI.join(),
                resolution: this.resolution.value,
                effect: checkboxesEffects.join(),
            },
            //data: new FormData(this),
            success: function(msg) {
                console.log(msg);
                if (msg == 'ok') {
                    alert('Success');
                    $('#form').trigger('reset'); // очистка формы
                } else {
                    alert('Error');
                }
            }
        });
    }


});
