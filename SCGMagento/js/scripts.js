/* eslint prefer-arrow-callback: 0 */

$(document).ready(function () {
    $(".service_orders__tr").on("click", function () {
        $(this).find(".service_orders_openclose").toggleClass("opened");
        $(this).closest(".service_orders__tr").find(".service_order_collapse").slideToggle();
    });

    $(".asidelinks").on("click", function () {
        $(this).toggleClass("opened");
    });

    $(".custom_select").selectmenu();
    $(".datepicker input").datepicker();

    $(".custom_select2").select2({
        minimumResultsForSearch: Infinity,
        theme: "CustomSelect2",
        templateSelection: selectCard,
        templateResult: selectCard,
    });

    $(".custom_select3").select2({
        minimumResultsForSearch: Infinity,
        theme: "CustomSelect3",
    });

    $(".custom_select4").select2({
        theme: "CustomSelect4",
    });

    $("#cardselect").on("change", function () {
        var CurrentCard = $(this).val();
        $(".cardselect_nav").find('a[href="#'+ CurrentCard +'"]').tab('show');
    });


    $(".orderinfo_opener").on("click", function () {
        $(".orderinfo").slideDown();
        $('html, body').animate({scrollTop: $('.orderinfo').offset().top - 100}, 1000);
    });

    $(".search_item_form_quanity .inc").on("click", function () {
        var curr_val = $(this).parents(".search_item_form_quanity").find("input").val();
        var curr_cost = $(this).closest(".search_item").find(".search_item_cost_curr").html();
        if (curr_val < 999) {
            ++curr_val;
            $(this).parents(".search_item_form_quanity").find("input").val(curr_val);
        }
        var curr_total = (curr_val * curr_cost).toFixed(2);
        $(this).closest(".search_item_form").find(".search_item_input_stock").val(curr_total);
    });

    $(".search_item_form_quanity .dec").on("click", function () {
        var curr_val = $(this).parents(".search_item_form_quanity").find("input").val();
        var curr_cost = $(this).closest(".search_item").find(".search_item_cost_curr").html();
        if (curr_val > 1) {
            --curr_val;
            $(this).parents(".search_item_form_quanity").find("input").val(curr_val);
        }
        var curr_total = (curr_val * curr_cost).toFixed(2);
        $(this).closest(".search_item_form").find(".search_item_input_stock").val(curr_total);
    });

    $(".receipt_opener").on("click", function () {
        $(this).toggleClass("opened");
        $(this).closest(".receipt").find(".receipt_content").slideToggle()
    });

    $(".with_menu3").on("click", function () {
        $(this).toggleClass("opened");
        $(this).find(".main_menu_3").slideToggle()
    });

    $(".header_left_menu").on("click", function () {
        $(".main_menu").fadeToggle();
    });

    $(".main_menu_1").on("click", function () {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $(this).closest(".main_menu_row_col").find(".main_menu_2").removeClass("opened");
        }
        else {
            $(".main_menu_1").removeClass("active");
            $(this).addClass("active");
            $(".main_menu_2").removeClass("opened");
            $(this).closest(".main_menu_row_col").find(".main_menu_2").addClass("opened");
        }
    });

    $(".notification_popup_markall").on("click", function () {
        $(this).fadeOut();
        $(".notification_popup_item").addClass("readed");
    });

    $(".header_notification_opener").on("click", function () {
        $(".header_notification_popup_bg").show()
        $(".header_notification_popup").fadeIn();
    });

    $(".header_notification_popup_bg").on("click", function () {
        $(".header_notification_popup, .header_notification_popup_bg").fadeOut();
    });

    $(".payment_information_table_opener").on("click", function () {
        $(this).parent(".payment_information_table_group").toggleClass("opened");
        $(this).parent(".payment_information_table_group").children(".payment_information_table_content").slideToggle(150);
    });

    $(".payment_billingaddress_opener").on("click", function () {
        $(this).parent(".payment_billingaddress").toggleClass("opened");
        $(this).parent(".payment_billingaddress").find(".payment_billingaddress_content").slideToggle(150);
    });

    $(".payment_details_item_title").on("click", function () {
        $(this).parent(".payment_rightpanel_group").toggleClass("opened");
        $(this).parent(".payment_rightpanel_group").find(".payment_rightpanel_content").slideToggle(150);
    });

    $(".paymentinformation_opener").on("click", function () {
        $(this).parent(".paymentinformation").toggleClass("opened");
        $(this).parent(".paymentinformation").find(".paymentinformation_content").slideToggle(150);
    });

    $(".payment_addfiles input").change(function(){
        var filename = $(this).val().replace(/.*\\/, "");
        $(".payment_addfiles .title").text(filename);
    });

    $(".paymentinformation_card_new .paymentinformation_card_number_input").on("input",function(){
        if($(this).val().length > 0) {
            $(".paymentinformation_card_new").addClass("paymentinformation_card_new_mastercard")
        } else  {
            $(".paymentinformation_card_new").removeClass("paymentinformation_card_new_mastercard")
        }
    });


    $(".lenticular_preview_image").on("click", function () {
        let currentImage = $(this).find("img").attr("src");
        $(".lenticular_preview_bigimg").css("background-image", "url("+currentImage+")" )

    });

    $(".boxestype").on("click", function () {
        let currentBoxType =  $(this).find("input").attr("value");
        $(".shippingboxes_opener").attr("data-target", "#" + currentBoxType)
    });

    $('.popup_trackinventory_checkbox').on('click', function () {
        $('.popup_trackinventory').toggleClass('opened');
    });

    $('.popup_newitem_returnable_opener .check').on('click', function () {
        $('.popup_newitem_returnable').toggleClass('opened');
    });
    //


});


function selectCard(card) {
    if (!card.id) {
        return card.text;
    }

    if (card.element.getAttribute('data-card').length > 0) {
        var $card = $(
            '<div class="selectcardli">'
            +'<span class="selectcardli_icon ' + card.element.getAttribute('data-card') + '">'
            + '</span>'
            + card.text
            + '</div>'
        );
    } else {
        var $card = $(
            '<div class="selectcardli">'
            + card.text
            + '</div>'
        );
    }
    return $card;
};


$(window).on("load resize", function () {
    if($(".lenticular_preview_images_slider").height() > $(".lenticular_preview_images_scroll").height()) {
        $(".lenticular_preview_images").addClass("scroll")
    } else {
        $(".lenticular_preview_images").removeClass("scroll")
    }


    $(".lenticular_preview_images_top").on("click", function () {
        let currentScrollPosition = $(".lenticular_preview_images_scroll").scrollTop();
        $('.lenticular_preview_images_scroll').animate({
            scrollTop: currentScrollPosition - 136,
        });
    });
    $(".lenticular_preview_images_bottom").on("click", function () {
        let currentScrollPosition = $(".lenticular_preview_images_scroll").scrollTop();
        $('.lenticular_preview_images_scroll').animate({
            scrollTop: currentScrollPosition + 136,
        });
    });
    $(".lenticular_preview_images_left").on("click", function () {
        let currentScrollPosition = $(".lenticular_preview_images_scroll").scrollLeft();
        $('.lenticular_preview_images_scroll').animate({
            scrollLeft: currentScrollPosition - 120,
        });
    });
    $(".lenticular_preview_images_right").on("click", function () {
        let currentScrollPosition = $(".lenticular_preview_images_scroll").scrollLeft();
        $('.lenticular_preview_images_scroll').animate({
            scrollLeft: currentScrollPosition + 120,
        });
    });


});

